import React from 'react'
import PropTypes from 'prop-types'

import './styles.scss'

export const Heading = ({ lavel, children }) => {
  const HeadingLevel = lavel

  return <HeadingLevel>{children}</HeadingLevel>
}

Heading.propTypes = {
  lavel: PropTypes.string,
  children: PropTypes.any.isRequired
}

Heading.defaultProps = {
  lavel: 'h1'
}
